import numpy as np
import pandas as pd
from sklearn.preprocessing import Imputer
"""
Country,Age,Salary,Purchased
France,44,72000,No
Spain,27,48000,Yes
Germany,30,54000,No
Spain,38,61000,No
Germany,40,,Yes
France,35,58000,Yes
Spain,,52000,No
France,48,79000,Yes
Germany,50,83000,No
France,37,67000,Yes
"""
dataset = pd.read_csv('datasets\Data.csv')
X = dataset.iloc[ : , :-1].values # remove last elm (:-1)
Y = dataset.iloc[ : , 3].values # get 3th elm
print(X.values)
# print(Y)
imputer = Imputer(missing_values = "NaN", strategy = "mean", axis = 0)
imputer = imputer.fit(X[ : , 1:3]) # get elm from index 1, to index 2 (3-1=2)
# X[ : , 1:3] = imputer.transform(X[ : , 1:3])
X = imputer.transform(X[ : , 1:3])

# print(X)
